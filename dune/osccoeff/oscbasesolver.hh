#ifndef OSC_BASE_SOLVER_HH
#define OSC_BASE_SOLVER_HH

#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/iterationsteps/cgstep.hh>
#include <dune/solvers/norms/energynorm.hh>

/** \brief A conjugate gradient solver 
 *
 */
template <class MatrixType, class VectorType>
class OscBaseSolver : public Dune::Solvers::LoopSolver<VectorType> {

protected:
    using Base = Dune::Solvers::LoopSolver<VectorType>;

private:
    Dune::Solvers::CGStep<MatrixType, VectorType> cgStep_;
    EnergyNorm<MatrixType, VectorType> norm_;

public:     

    /** \brief Constructor taking all relevant data */
    OscBaseSolver(int maxIterations, double tolerance, Solver::VerbosityMode verbosity = Solver::QUIET) :
        cgStep_(),
        norm_(cgStep_),
        Base(&cgStep_, maxIterations, tolerance, &norm_, verbosity)
    {}
    
    void setProblem(const MatrixType& mat, VectorType& x, const VectorType& rhs)
    {
        cgStep_.setProblem(mat, x, rhs);
        norm_.setMatrix(&mat);
        this->errorNorm_ = &norm_;
    }
};

#endif
