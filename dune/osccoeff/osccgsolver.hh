#ifndef CG_SOLVER_HH
#define CG_SOLVER_HH

#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/iterationsteps/lineariterationstep.hh>
#include <dune/solvers/norms/norm.hh>
#include <dune/solvers/transferoperators/compressedmultigridtransfer.hh>

/** \brief A conjugate gradient solver 
 *
 */
template <class MatrixType, class VectorType>
class OscCGSolver : public Dune::Solvers::IterativeSolver<VectorType>
{
    static const int blocksize = VectorType::block_type::dimension;

public:     

    /** \brief Constructor taking all relevant data */
        
    OscCGSolver(const MatrixType* matrix,
             VectorType* x,
             VectorType* rhs,
             LinearIterationStep<MatrixType,VectorType>* preconditioner,
             int maxIterations,
             double tolerance,
             Norm<VectorType>* errorNorm,
             NumProc::VerbosityMode verbosity,
             VectorType* exactSol,
	     VectorType* fineSol,
	     CompressedMultigridTransfer<VectorType>* mgTransfer,
	     double terminationFactor,
             bool useRelativeError=true)
      : Dune::Solvers::IterativeSolver<VectorType>(tolerance,maxIterations,verbosity,useRelativeError),
          matrix_(matrix), x_(x), rhs_(rhs), exactSol_(exactSol), fineSol_(fineSol), mgTransfer_(mgTransfer),
          preconditioner_(preconditioner), errorNorm_(errorNorm), terminationFactor_(terminationFactor)
    {}
    
    /** \brief Loop, call the iteration procedure
     * and monitor convergence */
    virtual void solve();
    
    /** \brief Checks whether all relevant member variables are set
     * \exception SolverError if the iteration step is not set up properly
     */
    virtual void check() const;
    
    void print(const MatrixType& mat, std::string message);
    void print(const VectorType& x, std::string message);

    const MatrixType* matrix_;
    VectorType* x_;
    VectorType* rhs_;
       
    VectorType* exactSol_;
    VectorType* fineSol_;

    CompressedMultigridTransfer<VectorType>* mgTransfer_;
    //! The iteration step used by the algorithm
    LinearIterationStep<MatrixType,VectorType>* preconditioner_;

    //! The norm used to measure convergence
    Norm<VectorType>* errorNorm_;
    
    double terminationFactor_;
};

#include "osccgsolver.cc"

#endif
