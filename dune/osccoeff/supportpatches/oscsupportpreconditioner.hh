#ifndef OSC_PRECONDITIONER_HH
#define OSC_PRECONDITIONER_HH

#include <queue>
#include <string>

#include <dune/common/timer.hh>
#include <dune/common/fvector.hh>

#include <dune/solvers/iterationsteps/lineariterationstep.hh>

#include <dune/osccoeff/compressedmultigridtransfer.hh>
#include <dune/osccoeff/supportpatches/localpatchfactory.hh>
#include <dune/osccoeff/osclocalproblem.hh>
#include <dune/osccoeff/hierarchicleveliterator.hh>
#include <dune/osccoeff/oscrhs.hh>
#include <dune/osccoeff/oscbasesolver.hh>

#include <dune/fufem/assemblers/assembler.hh>
#include <dune/fufem/assemblers/localassemblers/l2functionalassembler.hh>
#include <dune/fufem/boundarypatch.hh>

#include <dune/grid/common/mcmgmapper.hh>
#include <dune/grid/common/intersectioniterator.hh>

#include <dune/istl/superlu.hh>

template <class GridType, class BasisType, class MatrixType, class VectorType>
class OscSupportPreconditioner : public LinearIterationStep<MatrixType, VectorType> {

public:
    typedef GridType grid_type;
    typedef MatrixType matrix_type;

private:
    static const int dim= GridType::dimension;
    static const int dimworld = GridType::dimensionworld;
    typedef typename GridType::ctype ctype;
    typedef typename Dune::FieldVector<ctype, dimworld> WorldVectorType;
    typedef typename Dune::LevelMultipleCodimMultipleGeomTypeMapper <GridType,  Dune::MCMGElementLayout > MapperType;
    typedef typename GridType::template Codim<0>::Entity EntityType;

    typedef HierarchicLevelIterator<GridType> HierarchicLevelIteratorType;

    typedef OscBaseSolver<MatrixType, VectorType> BaseSolver;

    GridType& grid_;
    std::vector<ctype>& oscData_;
    
    const int coarseLevel_;
    const int fineLevel_;
    
    MapperType& mapper_;
    
    const int patchDepth_;

    MatrixType& fineMat_;
    VectorType& fineRhs_;
    
    BaseSolver& baseSolver_;

    const bool debugMode_;

    std::vector<OscLocalProblem<MatrixType, VectorType>* > localProblems_;

    MatrixType coarseStiffMat_;
    VectorType coarseRhs_;
    Dune::BitSetVector<1> coarseBoundaryDofs_;
    CompressedMultigridTransfer<VectorType> mgTransfer_;

public:

    OscSupportPreconditioner(GridType& grid, int coarseLevel, int fineLevel, std::vector<ctype>& oscData, MapperType& mapper, int patchDepth, MatrixType& fineMat, VectorType& fineRhs, BaseSolver& baseSolver, const bool debugMode)
        : grid_(grid), oscData_(oscData), coarseLevel_(coarseLevel), fineLevel_(fineLevel), mapper_(mapper), patchDepth_(patchDepth), fineMat_(fineMat), fineRhs_(fineRhs), baseSolver_(baseSolver), debugMode_(debugMode) {

        typedef typename GridType::LevelGridView GV;
        typedef typename BasisType::LocalFiniteElement FE;

        GV coarseGridView(grid_.levelGridView(coarseLevel_));
        GV fineGridView(grid_.levelGridView(fineLevel_));

        BasisType coarseBasis(coarseGridView);
        BasisType fineBasis(fineGridView);

        std::cout << "Setting coarse grid oscData:" << std::endl;
        Dune::LevelMultipleCodimMultipleGeomTypeMapper <GridType,  Dune::MCMGElementLayout > coarseMapper(grid_, coarseLevel_);

    	// allocate  a  vector  for  the  data
    	std::vector<ctype> coarseOscData(coarseMapper.size());

        // init vertexInElements
        std::vector<std::vector<EntityType>> vertexInElements;
        vertexInElements.resize(coarseBasis.size());
        for (size_t i=0; i<vertexInElements.size(); i++) {
            vertexInElements[i].resize(0);
        }

        typedef  typename GV::template Codim <0>::Iterator  ElementLevelIterator;
    	ElementLevelIterator endElemIt = coarseGridView.template end <0>();
    	for (ElementLevelIterator  it = coarseGridView. template begin <0>(); it!=endElemIt; ++it) {

            // compute coarseGrid vertexInElements
            const FE& coarseFE = coarseBasis.getLocalFiniteElement(*it);

            for (size_t i=0; i<coarseFE.localBasis().size(); ++i) {
                int dofIndex = coarseBasis.index(*it, i);
                vertexInElements[dofIndex].push_back(*it);
            }

            // set coarseOscData
            int counter = 0;
            ctype sum = 0;

            HierarchicLevelIteratorType endHierIt(grid_, *it, HierarchicLevelIteratorType::PositionFlag::end, fineLevel_);
            for (HierarchicLevelIteratorType hierIt(grid_, *it, HierarchicLevelIteratorType::PositionFlag::begin, fineLevel_); hierIt!=endHierIt; ++hierIt) {

                sum += oscData_[mapper_.index(*hierIt)];
                counter++;
            }
            coarseOscData[coarseMapper.index(*it)] = sum/counter;
        } 

        std::cout << "Assembling coarse grid stiffness matrix!" << std::endl;

        // assemble coarse stiffness matrix
        Assembler<BasisType, BasisType> coarseAssembler(coarseBasis, coarseBasis);
        OscLocalAssembler<GridType, FE, FE> coarseLocalAssembler(coarseOscData, coarseMapper);
        coarseAssembler.assembleOperator(coarseLocalAssembler, coarseStiffMat_);
    
    
        std::cout << "Setting coarse grid boundary conditions!" << std::endl;

        typedef typename MatrixType::row_type RowType;
        typedef typename RowType::ConstIterator ColumnIterator;

        // set coarse boundary conditions
        BoundaryPatch<typename GridType::LevelGridView> coarseBoundaryPatch(coarseGridView, true);
        constructBoundaryDofs(coarseBoundaryPatch, coarseBasis, coarseBoundaryDofs_);
        
        for(size_t i=0; i<coarseBoundaryDofs_.size(); i++) {
            if(!coarseBoundaryDofs_[i][0])
                continue;

            const RowType& row = coarseStiffMat_[i];

            ColumnIterator cIt    = row.begin();
            ColumnIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt) {
                coarseStiffMat_[i][cIt.index()] = 0;
            }
            coarseStiffMat_[i][i]=1;
        }

        mgTransfer_.setup(coarseGridView, fineGridView);
       
        localProblems_.resize(grid_.size(coarseLevel_, dim));

        std::cout << "Initializing local fine grid corrections! " << std::endl;

        // init local fine level corrections
        Timer timer;
        timer.reset();
        timer.start();
        for (size_t i=0; i<vertexInElements.size(); i++) {
            LocalPatchFactory<BasisType> patchFactory(coarseBasis, fineBasis, coarseLevel_, fineLevel_, vertexInElements, i, patchDepth_);
            std::vector<int>& localToGlobal = patchFactory.getLocalToGlobal();
            Dune::BitSetVector<1>& boundaryDofs = patchFactory.getBoundaryDofs();

            VectorType rhs;
            rhs.resize(fineMat_.N());
            rhs = 0;

            localProblems_[i] = new OscLocalProblem<MatrixType, VectorType>(fineMat_, rhs, localToGlobal, boundaryDofs, baseSolver_, false);

            if ((i+1) % 10 == 0) {
                std::cout << '\r' << std::floor((i+1.0)/coarseBasis.size()*100) << " % done. Elapsed time: " << timer.elapsed() << " seconds. Predicted total setup time: " << timer.elapsed()/(i+1.0)*coarseBasis.size() << " seconds." << std::flush;
            }
        }

        std::cout << std::endl;
        std::cout << "Total setup time: " << timer.elapsed() << " seconds." << std::endl;
        timer.stop();
    }


   virtual void setProblem(const MatrixType& mat, VectorType& x, const VectorType& rhs) {
        this->x_ = &x;
        this->rhs_ = &rhs;
        this->mat_ = Dune::stackobject_to_shared_ptr(mat);

        mgTransfer_.restrict(*(this->rhs_), coarseRhs_);
        for(size_t i=0; i<coarseBoundaryDofs_.size(); i++) {
            if(!coarseBoundaryDofs_[i][0])
                continue;

            coarseRhs_[i] = 0;
        }

        for (size_t i=0; i<localProblems_.size(); i++) {
            localProblems_[i]->updateRhs(*(this->rhs_));
        }
    }

    virtual void iterate() {

	    // compute exact coarse solution
        *(this->x_) = 0;	

        VectorType coarseSol(coarseStiffMat_.M());

        #if HAVE_SUPERLU
        Dune::InverseOperatorResult res;

        Dune::SuperLU<MatrixType> coarseSolver(coarseStiffMat_);
        coarseSolver.apply(coarseSol, coarseRhs_, res);
        #else
        baseSolver_.setProblem(coarseStiffMat_, coarseSol, coarseRhs_);
        baseSolver_.preprocess();
        baseSolver_.solve();
        #endif

    	mgTransfer_.prolong(coarseSol, *(this->x_));

        VectorType it, x;
        for (size_t i=0; i<localProblems_.size(); i++) {
      
            localProblems_[i]->solve(it);
            localProblems_[i]->prolong(it, x);

            *(this->x_) += x;
        }
    }
};

#endif

