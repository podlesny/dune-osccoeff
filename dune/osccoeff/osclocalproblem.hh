#ifndef OSC_LOCAL_PROBLEM_HH
#define OSC_LOCAL_PROBLEM_HH

#include <math.h>   
#include <dune/common/fmatrix.hh>
#include <dune/common/function.hh>
#include <dune/common/timer.hh>

#include <dune/istl/matrixindexset.hh>
#include <dune/istl/superlu.hh>

#include <dune/fufem/assemblers/localoperatorassembler.hh>

#include <dune/osccoeff/oscbasesolver.hh>

template <class MatrixType, class DomainType, class RangeType = DomainType>
class OscLocalProblem {
  
private:
    typedef typename MatrixType::block_type block_type;
    typedef typename MatrixType::field_type ctype;

    typedef OscBaseSolver<MatrixType, DomainType> BaseSolver;

    const MatrixType& mat_;
    const RangeType& rhs_;

    const std::vector<int> dofs_; // local to global map
    const Dune::BitSetVector<1> boundaryDofs_; // local boundary dofs

    BaseSolver& baseSolver_;

    MatrixType localMat;
    RangeType localRhs;

    const bool debugMode_;
public:
  //  template <BasisType>
    OscLocalProblem(const MatrixType& mat, const RangeType& rhs, const std::vector<int> dofs, const Dune::BitSetVector<1> boundaryDofs,
            BaseSolver& baseSolver,
		    bool debugMode=false)
        : mat_(mat),
	  rhs_(rhs),
	  dofs_(dofs),
	  boundaryDofs_(boundaryDofs),
      baseSolver_(baseSolver),
	  debugMode_(debugMode)
	{

	if (dofs_.size()!=boundaryDofs_.size())
	    DUNE_THROW(Dune::Exception, "Sizes of dofs and boundaryDofs do not match!");
	  
	// construct globalToLocal map
	std::map<int, int> globalToLocal;
	for (size_t i=0; i<dofs_.size(); ++i) {
	    globalToLocal[dofs_[i]] = i;
	}

	// build local stiffness matrix
        int localDim = dofs_.size();
        Dune::MatrixIndexSet localIdxSet(localDim, localDim);
	
	typedef typename MatrixType::row_type RowType;
	typedef typename RowType::ConstIterator ColumnIterator;

	for(size_t rowIdx = 0; rowIdx<dofs_.size(); rowIdx++) {    
	    const int globalRowIdx = dofs_[rowIdx]; 
	    const RowType& row = mat_[globalRowIdx];
	    
	    ColumnIterator colIt = row.begin();
	    const ColumnIterator colEndIt = row.end();
            for(; colIt!=colEndIt; ++colIt) {
		const int globalColIdx = colIt.index(); 
		if (*colIt!=0 and globalToLocal.count(globalColIdx)!=0) {
		    localIdxSet.add(rowIdx, globalToLocal[globalColIdx]);
		}
            }
        } 

        localIdxSet.exportIdx(localMat);

        for(size_t rowIdx = 0; rowIdx<localMat.N(); rowIdx++) {
            RowType& row = localMat[rowIdx];
	    const RowType& globalRow = mat_[dofs_[rowIdx]];
	    
            ColumnIterator cIt    = row.begin();
            ColumnIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt) {	
		row[cIt.index()] = globalRow[dofs_[cIt.index()]];
            }
	} 	

	// init local rhs
	localRhs.resize(dofs_.size());
        localRhs = 0;

	// set local rhs and boundary dofs
	for (size_t i=0; i<boundaryDofs_.size(); i++) {

            if(!boundaryDofs_[i][0]) {
		localRhs[i] = rhs_[dofs_[i]];
		continue;
	    }
			  
            RowType& row = localMat[i];

            ColumnIterator cIt    = row.begin();
            ColumnIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt) {
                row[cIt.index()] = 0;
            }
            
            row[i] = 1;
	}
    }

    MatrixType& getLocalMat() {
	return localMat;
    }
    
    void setRhs(const RangeType& d){
	localRhs.resize(d.size());
	
	for (size_t i=0; i<localRhs.size(); ++i) {
	    localRhs[i] = d[i];
	}
    }
    
    void updateRhs(const RangeType& d){
        restrict(d, localRhs);
    }

    void solve(DomainType& x){
        RangeType localRhsCopy(localRhs);
        x.resize(localMat.M());

        #if HAVE_SUPERLU
        Dune::InverseOperatorResult res;

        Dune::SuperLU<MatrixType> directSolver(localMat);
        directSolver.apply(x, localRhsCopy, res);
       #else
       baseSolver_.setProblem(localMat, x, localRhs);
       baseSolver_.preprocess();
       baseSolver_.solve();
       #endif
    }
    
    void prolong(DomainType& x, DomainType& res){
        res.resize(mat_.M());
        res = 0;

        for (size_t i=0; i<x.size(); i++) {
	    if (!boundaryDofs_[i][0])
                res[dofs_[i]] = x[i];
        }
    }

    void restrict(const RangeType& x, RangeType& res){
        res.resize(dofs_.size());
        res = 0;

        for (size_t i=0; i<res.size(); i++) {

            if (!boundaryDofs_[i][0])
                res[i] = x[dofs_[i]];
        }

        if (debugMode_) {
         //   print(x, "globalRhs: ");
         //   print(res, "restrictedRhs: ");
        }
    }
};

#endif
