#ifndef OSC_UNITCUBE_HH
#define OSC_UNITCUBE_HH

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

// default implementation for any template parameter
template<typename T, int variant>
class OscUnitCube 
{
public:
  typedef T GridType;
  
  static const int dim = GridType::dimension;

  // constructor throwing exception
  OscUnitCube (int i = 1)
  {
	  Dune::FieldVector<typename GridType::ctype,dim> lowerLeft(0);
      Dune::FieldVector<typename GridType::ctype,dim> upperRight(1);
      std::array<unsigned int,dim> elements;
      std::fill(elements.begin(), elements.end(), i);
      
      switch (variant) {
        case 1:
        grid_ = Dune::StructuredGridFactory<GridType>::createCubeGrid(lowerLeft, upperRight, elements);
        break;
        case 2:
        grid_ = Dune::StructuredGridFactory<GridType>::createSimplexGrid(lowerLeft, upperRight, elements);
        break;
        default:  
        DUNE_THROW( Dune::NotImplemented, "Variant " 
                  << variant << " of unit cube not implemented." );
    }
  }

  T& grid ()
  {
	return *grid_;
  }

private:
  // the constructed grid object
  Dune::shared_ptr<T> grid_;
};

// include specializations
#include"oscunitcube_alugrid.hh"

#endif
