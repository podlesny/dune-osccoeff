#ifndef OSC_OPERATOR_ASSEMBLER_HH
#define OSC_OPERATOR_ASSEMBLER_HH

#include <dune/istl/matrix.hh>
#include <dune/istl/matrixindexset.hh>

#include "dune/fufem/functionspacebases/functionspacebasis.hh"
#include "dune/fufem/staticmatrixtools.hh"

//! Generic global assembler for operators on a gridview
template <class TrialBasis, class AnsatzBasis>
class OscOperatorAssembler
{
    private:
        typedef typename TrialBasis::GridView GridView;
        typedef typename GridView::Grid GridType;
        typedef typename GridType::template Codim<0>::Entity EntityType;

    public:
        //! create assembler for grid
        OscOperatorAssembler(const TrialBasis& tBasis, const AnsatzBasis& aBasis, std::vector<EntityType>& regionElements) :
            tBasis_(tBasis),
            aBasis_(aBasis),
            regionElements_(regionElements)
        {}


        template <class LocalAssemblerType>
        void addIndices(LocalAssemblerType& localAssembler, Dune::MatrixIndexSet& indices, const bool lumping=false) const
        {
            if (lumping)
                addIndicesStaticLumping<LocalAssemblerType,true>(localAssembler, indices);
            else
                addIndicesStaticLumping<LocalAssemblerType,false>(localAssembler, indices);
        }


        template <class LocalAssemblerType, class GlobalMatrixType>
        void addEntries(LocalAssemblerType& localAssembler, GlobalMatrixType& A, const bool lumping=false) const
        {
            if (lumping)
                addEntriesStaticLumping<LocalAssemblerType,GlobalMatrixType,true>(localAssembler, A);
            else
                addEntriesStaticLumping<LocalAssemblerType,GlobalMatrixType,false>(localAssembler, A);
        }


        template <class LocalAssemblerType, class GlobalMatrixType>
        void assemble(LocalAssemblerType& localAssembler, GlobalMatrixType& A, const bool lumping=false) const
        {
            int rows = tBasis_.size();
            int cols = aBasis_.size();

            Dune::MatrixIndexSet indices(rows, cols);

            addIndices(localAssembler, indices, lumping);

            indices.exportIdx(A);
            A=0.0;

            typedef typename GlobalMatrixType::row_type RowType;
            typedef typename RowType::ConstIterator ColumnIterator;

            for(size_t rowIdx = 0; rowIdx<A.N(); rowIdx++) {
                const RowType& row = A[rowIdx];
                        
                ColumnIterator cIt    = row.begin();
                ColumnIterator cEndIt = row.end();
            
                for(; cIt!=cEndIt; ++cIt) {
                  //  std::cout << rowIdx << " " << cIt.index() << " " << A[rowIdx][cIt.index()] << std::endl;
            }
    } 


            addEntries(localAssembler, A, lumping);

            return;
        }


    protected:

        template <class LocalAssemblerType, bool lumping>
        void addIndicesStaticLumping(LocalAssemblerType& localAssembler, Dune::MatrixIndexSet& indices) const
        {
            typedef typename LocalAssemblerType::BoolMatrix BoolMatrix;
            typedef typename TrialBasis::LinearCombination LinearCombination;

            for (size_t i=0; i<regionElements_.size(); ++i)
            {
                EntityType it = regionElements_[i];

                // get shape functions
                const typename TrialBasis::LocalFiniteElement& tFE = tBasis_.getLocalFiniteElement(it);
                const typename AnsatzBasis::LocalFiniteElement& aFE = aBasis_.getLocalFiniteElement(it);

                BoolMatrix localIndices(tFE.localBasis().size(), aFE.localBasis().size());
                localAssembler.indices(it, localIndices, tFE, aFE);

                for (size_t i=0; i<tFE.localBasis().size(); ++i)
                {
                    int rowIndex = tBasis_.index(it, i);
                    const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                    bool rowIsConstrained = tBasis_.isConstrained(rowIndex);
                    for (size_t j=0; j<aFE.localBasis().size(); ++j)
                    {
                        if (localIndices[i][j])
                        {
                            if (lumping)
                            {
                                if (rowIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                        indices.add(rowConstraints[rw].index, rowConstraints[rw].index);
                                }
                                else
                                    indices.add(rowIndex, rowIndex);
                            }
                            else
                            {
                                int colIndex = aBasis_.index(it, j);
                                const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                                bool colIsConstrained = aBasis_.isConstrained(colIndex);
                                if (rowIsConstrained and colIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                    {
                                        for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                            indices.add(rowConstraints[rw].index, colConstraints[cw].index);
                                    }
                                }
                                else if (rowIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                        indices.add(rowConstraints[rw].index, colIndex);
                                }
                                else if (colIsConstrained)
                                {
                                    for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                        indices.add(rowIndex, colConstraints[cw].index);
                                }
                                else
                                    indices.add(rowIndex, colIndex);
                            }
                        }
                    }
                }
            }
        }


        template <class LocalAssemblerType, class GlobalMatrixType, bool lumping>
        void addEntriesStaticLumping(LocalAssemblerType& localAssembler, GlobalMatrixType& A) const
        {
            typedef typename LocalAssemblerType::LocalMatrix LocalMatrix;
            typedef typename TrialBasis::LinearCombination LinearCombination;

            for (size_t i=0; i<regionElements_.size(); ++i)
            {
                EntityType it = regionElements_[i];

                // get shape functions
                const typename TrialBasis::LocalFiniteElement& tFE = tBasis_.getLocalFiniteElement(it);
                const typename AnsatzBasis::LocalFiniteElement& aFE = aBasis_.getLocalFiniteElement(it);

                LocalMatrix localA(tFE.localBasis().size(), aFE.localBasis().size());
                localAssembler.assemble(it, localA, tFE, aFE);

                for (size_t i=0; i<tFE.localBasis().size(); ++i)
                {
                    int rowIndex = tBasis_.index(it, i);
                    const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                    bool rowIsConstrained = tBasis_.isConstrained(rowIndex);
                    for (size_t j=0; j<aFE.localBasis().size(); ++j)
                    {

                        if (localA[i][j].infinity_norm()!=0.0)
                        {
                            if (lumping)
                            {
                                if (rowIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
				        Arithmetic::addProduct(A[rowConstraints[rw].index][rowConstraints[rw].index], rowConstraints[rw].factor, localA[i][j]);
                                }
                                else
                                    Arithmetic::addProduct(A[rowIndex][rowIndex], 1.0, localA[i][j]);
                            }
                            else
                            {
                                int colIndex = aBasis_.index(it, j);
                                const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                                bool colIsConstrained = aBasis_.isConstrained(colIndex);
                                if (rowIsConstrained and colIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                    {
                                        for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                            Arithmetic::addProduct(A[rowConstraints[rw].index][colConstraints[cw].index], rowConstraints[rw].factor * colConstraints[cw].factor, localA[i][j]);
                                    }
                                }
                                else if (rowIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                        Arithmetic::addProduct(A[rowConstraints[rw].index][colIndex], rowConstraints[rw].factor, localA[i][j]);
                                }
                                else if (colIsConstrained)
                                {
                                    for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                        Arithmetic::addProduct(A[rowIndex][colConstraints[cw].index], colConstraints[cw].factor, localA[i][j]);
                                }
                                else
                                    Arithmetic::addProduct(A[rowIndex][colIndex], 1.0, localA[i][j]);
                            }
                        }
                    }
                }
            }
        }


        const TrialBasis& tBasis_;
        const AnsatzBasis& aBasis_;
	std::vector<EntityType>& regionElements_;
};

#endif

