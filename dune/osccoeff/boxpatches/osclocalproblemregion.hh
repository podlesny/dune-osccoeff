#ifndef OSC_LOCAL_PROBLEM_REGION_HH
#define OSC_LOCAL_PROBLEM_REGION_HH

#include <dune/common/function.hh>

template <class ElementType, class VectorType> 
class OscLocalProblemRegion : public Dune::VirtualFunction<ElementType, bool> {
    public:
        OscLocalProblemRegion() {};

        virtual void evaluate(const ElementType& element, bool& y) const=0;
};

template <class ElementType, class VectorType>
class OscLocalProblemRegionBox : public OscLocalProblemRegion<ElementType, VectorType> {
    private:
    typedef typename VectorType::field_type ctype;
    const VectorType& midPoint_;
    const ctype& boxLength_;

    public:
        OscLocalProblemRegionBox(const VectorType& midPoint, const ctype& boxLength) : midPoint_(midPoint), boxLength_(boxLength) {};

        void evaluate(const ElementType& element, bool& inBox) const {
            VectorType center;
            center = element.geometry().center();
            center -= midPoint_;

            inBox = true;
            for (size_t i=0; i<center.size(); i++){
                if (std::abs(center[i]) > boxLength_) {
                    inBox = false;
                    break;
                }
            }
        }
};

#endif
