#ifndef OSC_ASSEMBLER_HH
#define OSC_ASSEMBLER_HH

#include "dune/osccoeff/boxpatches/oscoperatorassembler.hh"
#include "dune/osccoeff/boxpatches/oscfunctionalassembler.hh"

//#include "dune/fufem/assemblers/boundaryfunctionalassembler.hh"
//#include "dune/fufem/boundarypatch.hh"

//! Generic global assembler for operators and functionals
template <class TrialBasis, class AnsatzBasis>
class OscAssembler
{

    private:
        typedef typename TrialBasis::GridView::Grid GridType;
        typedef typename GridType::template Codim<0>::Entity EntityType;

        const TrialBasis& tBasis_;
        const AnsatzBasis& aBasis_;

        std::vector<EntityType>& regionElements_;

    public:
        //! create assembler
        OscAssembler(const TrialBasis& tBasis, const AnsatzBasis& aBasis, std::vector<EntityType>& regionElements) :
            tBasis_(tBasis),
            aBasis_(aBasis),
            regionElements_(regionElements) {}

        template <class LocalAssemblerType, class GlobalMatrixType>
        void assembleOperator(LocalAssemblerType& localAssembler, GlobalMatrixType& A, const bool lumping=false) const
        {
            const OscOperatorAssembler<TrialBasis, AnsatzBasis> operatorAssembler(tBasis_, aBasis_, regionElements_);
            operatorAssembler.assemble(localAssembler, A, lumping);
        }

        template <class LocalFunctionalAssemblerType, class GlobalVectorType>
        void assembleFunctional(LocalFunctionalAssemblerType& localAssembler, GlobalVectorType& b, bool initializeVector=true) const
        {
            const OscFunctionalAssembler<TrialBasis> functionalAssembler(tBasis_, regionElements_);
            functionalAssembler.assemble(localAssembler, b, initializeVector);
        }

        /*
        template <class LocalBoundaryFunctionalAssemblerType, class GlobalVectorType>
        void assembleBoundaryFunctional(LocalBoundaryFunctionalAssemblerType& localAssembler,
                                        GlobalVectorType& b,
                                        const BoundaryPatch<typename TrialBasis::GridView>& boundaryPatch,
                                        bool initializeVector=true) const
        {
            const BoundaryFunctionalAssembler<TrialBasis> boundaryFunctionalAssembler(tBasis_, boundaryPatch);
            boundaryFunctionalAssembler.assemble(localAssembler, b, initializeVector);
        }*/
};

#endif

