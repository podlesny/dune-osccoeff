#ifndef OSC_BOX_LOCAL_PROBLEM_HH
#define OSC_BOX_LOCAL_PROBLEM_HH

#include <math.h>   
#include <dune/common/fmatrix.hh>
#include <dune/common/function.hh>

#include <dune/istl/matrixindexset.hh>
#include <dune/istl/superlu.hh>

#include "dune/fufem/assemblers/localoperatorassembler.hh"
//#include "dune/fufem/assemblers/localfunctionalassembler.hh"

#include <dune/osccoeff/boxpatches/oscassembler.hh>
#include <dune/osccoeff/boxpatches/oscl2functionalassembler.hh>
#include <dune/osccoeff/oscbasesolver.hh>

template <class BasisType, class MatrixType, class DomainType, class RangeType>
class OscBoxLocalProblem {
private:
    typedef typename BasisType::GridView::Grid GridType;
    typedef typename GridType::template Codim<0>::Entity EntityType;
    typedef typename BasisType::LocalFiniteElement FE;
    typedef typename GridType::ctype ctype;
    typedef LocalOperatorAssembler <GridType, FE, FE, Dune::FieldMatrix<ctype,1,1>> LocalAssemblerType;
    typedef OscL2FunctionalAssembler <GridType, FE> LocalFunctionalAssemblerType;

    typedef OscBaseSolver<MatrixType, DomainType> BaseSolver;

    std::vector<EntityType>& regionElements_;
    BasisType& basis_; 
    Dune::BitSetVector<1>& boundaryDofs_;
    LocalAssemblerType& localOperatorAssembler_;
    RangeType& globalRhs_;
    LocalFunctionalAssemblerType& localFunctionalAssembler_; 
    
    BaseSolver& baseSolver_;

    MatrixType stiffnessMat_;
    RangeType rhs_;

    Dune::BitSetVector<1> localBoundaryDofs_;

    
    
    std::vector<int> localToGlobal_;

public:
  
    OscBoxLocalProblem(std::vector<EntityType>& regionElements, BasisType& basis, Dune::BitSetVector<1>& boundaryDofs,
                    LocalAssemblerType& localOperatorAssembler, RangeType& globalRhs, LocalFunctionalAssemblerType& localFunctionalAssembler, BaseSolver& baseSolver)
        : regionElements_(regionElements),
          basis_(basis),
          boundaryDofs_(boundaryDofs),
          localOperatorAssembler_(localOperatorAssembler),
          globalRhs_(globalRhs),
          localFunctionalAssembler_(localFunctionalAssembler),
          baseSolver_(baseSolver)
    {

	// assemble global stiffness matrix of local problem
        MatrixType globalStiffMat;
        OscAssembler<BasisType, BasisType> assembler(basis_, basis_, regionElements_);
        assembler.assembleOperator(localOperatorAssembler_, globalStiffMat);

	
	
 	// assemble global rhs for local problem        
	RangeType localRhs(globalRhs_.size());
	assembler.assembleFunctional(localFunctionalAssembler_, localRhs);

    typedef typename MatrixType::row_type RowType;
    typedef typename RowType::ConstIterator ColumnIterator;

    
	int localIdx = 0;
	localToGlobal_.resize(globalStiffMat.N());
        localBoundaryDofs_.resize(globalRhs_.size());
	localBoundaryDofs_.unsetAll();
	for (size_t i=0; i<localBoundaryDofs_.size(); i++) {
	    bool idxInMat = globalStiffMat.exists(i,i);

	    // set index transformation
	    if (idxInMat) {
		localToGlobal_[localIdx] = i;

	    	// set localBoundaryDofs_
            if(boundaryDofs_[i][0] or (!almost_equal(globalRhs_[i], localRhs[i], 2))) {
		    localBoundaryDofs_[localIdx][0] = true;
 
            		    
		    
            const RowType& row = globalStiffMat[i];

            ColumnIterator cIt    = row.begin();
            ColumnIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt) {
                globalStiffMat[i][cIt.index()] = 0;
            }
			
            	    globalStiffMat[i][i] = 1;
	        } 

		localIdx++;
	    }
	}

       // print(localBoundaryDofs_, "localBoundaryDofs:");
	
	localToGlobal_.resize(localIdx);	
	localBoundaryDofs_.resize(localIdx);

	// assemble local stiffness matrix
        int localDim = localToGlobal_.size();
        Dune::MatrixIndexSet localIdxSet(localDim, localDim);


	for(size_t rowIdx = 0; rowIdx<localToGlobal_.size(); rowIdx++) {    
	    const int globalRowIdx = localToGlobal_[rowIdx]; 
            for(size_t colIdx = 0; colIdx<localToGlobal_.size(); colIdx++) {
		const int globalColIdx = localToGlobal_[colIdx];
		if (globalStiffMat.exists(globalRowIdx, globalColIdx))
                   localIdxSet.add(rowIdx, colIdx);
            }
        } 

        localIdxSet.exportIdx(stiffnessMat_);

        for(size_t rowIdx = 0; rowIdx<stiffnessMat_.N(); rowIdx++) {
            const RowType& row = stiffnessMat_[rowIdx];

            ColumnIterator cIt    = row.begin();
            ColumnIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt) {
                stiffnessMat_[rowIdx] [cIt.index()] = globalStiffMat[localToGlobal_[rowIdx]][localToGlobal_[cIt.index()]];
            }
	} 

    }

    void updateRhs(const RangeType& d){
        restrict(d, rhs_);
    }

    void solve(DomainType& x){
        DomainType it(stiffnessMat_.M());

        #if HAVE_SUPERLU
        Dune::InverseOperatorResult res;

        Dune::SuperLU<MatrixType> directSolver(stiffnessMat_);
        directSolver.apply(it, rhs_, res);
        #else
        baseSolver_.setProblem(stiffnessMat_, it, rhs_);
        baseSolver_.preprocess();
        baseSolver_.solve();
        #endif

        prolong(it, x);
    }

private:
    typename std::enable_if<!std::numeric_limits<ctype>::is_integer, bool>::type
        almost_equal(ctype x, ctype y, int ulp)
    {
        return std::abs(x-y) < std::numeric_limits<ctype>::epsilon() * std::abs(x+y) * ulp
               || std::abs(x-y) < std::numeric_limits<ctype>::min();
    }

    void prolong(DomainType& x, DomainType& res){
        res.resize(globalRhs_.size());
        res = 0;

        for (size_t i=0; i<x.size(); i++) {
	    if (!localBoundaryDofs_[i][0])
                res[localToGlobal_[i]] = x[i];
        }
    }

    void restrict(const RangeType& x, RangeType& res){
        res.resize(localToGlobal_.size());
        res = 0;

        for (size_t i=0; i<res.size(); i++) {
	   if (!localBoundaryDofs_[i][0])
              res[i] = x[localToGlobal_[i]];
        }
    }
};

#endif
