#ifndef OSC_L2_FUNCTIONAL_ASSEMBLER_HH
#define OSC_L2_FUNCTIONAL_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/function.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

#include "dune/fufem/assemblers/localfunctionalassembler.hh"

#include <dune/osccoeff/boxpatches/osclocalproblemregion.hh>
#include <dune/osccoeff/oscrhs.hh>

/** \brief Local finite element assembler for L^2 functionals. */
template <class GridType, class TrialLocalFE, class T=Dune::FieldVector<double,1> >
class OscL2FunctionalAssembler :
    public LocalFunctionalAssembler<GridType, TrialLocalFE, T>

{
    private:
        typedef LocalFunctionalAssembler<GridType, TrialLocalFE, T> Base;
        static const int dim = GridType::dimension;
        static const int dimworld = GridType::dimensionworld;

        typedef typename GridType::ctype ctype;
        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;
        typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalCoordinate;
        typedef VirtualGridFunction<GridType, T> GridFunction;

    public:
        typedef typename Base::Element Element;
        typedef typename Base::Element::Geometry Geometry;
        typedef typename Base::LocalVector LocalVector;

        typedef typename Dune::VirtualFunction<GlobalCoordinate, T> Function;

        /**
         * \brief Create L2FunctionalAssembler
         *
         * Creates a local functional assembler for an L2-functional.
         * The QuadratureRuleKey given here does only specify what is
         * needed to integrate f. Depending on this the quadrature rule
         * is selected automatically such that fv for test functions v
         * can be integrated.
         * If you, e.g., specify quadrature order 1 for f and the test
         * functions need order 2 the selected quadrature rule will
         * have order 3.
         *
         * \param f The L2 function
         * \param fQuadKey A QuadratureRuleKey that specifies how to integrate f
         */
        OscL2FunctionalAssembler(const Function& f, const QuadratureRuleKey& fQuadKey) :
            f_(f),
            quadOrder_(-1),
            functionQuadKey_(fQuadKey)
        {}

        /**
         * \brief Create L2FunctionalAssembler
         *
         * Using this constructor the quadrature order will be selected
         * such that the test functions can be integrated exactly.
         *
         * \param f The L2 function
         */
        OscL2FunctionalAssembler(const Function& f, OscLocalProblemRegion<typename GridType::template Codim<0>::Entity, Dune::FieldVector<ctype, dimworld>>& region) :
            f_(f),
            quadOrder_(-1),
            functionQuadKey_(dim, 0),
            region_(region)
        {}

        //! create assembler for grid
        DUNE_DEPRECATED_MSG("Please select quadrature for L2FunctionalAssembler using a QuadratureRuleKey.")
        OscL2FunctionalAssembler(const Function& f, int order) :
            f_(f),
            quadOrder_(order),
            functionQuadKey_(dim, order)
        {}

        virtual void assemble(const Element& element, LocalVector& localVector, const TrialLocalFE& tFE) const
        {
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localVector = 0.0;

            bool inBox;
            region_.evaluate(element, inBox);
            if (!inBox)
            return;

            // get quadrature rule
            QuadratureRuleKey tFEquad(tFE);
            QuadratureRuleKey quadKey = tFEquad.product(functionQuadKey_);
            if (quadOrder_>=0)
                quadKey.setOrder(quadOrder_);
            const Dune::template QuadratureRule<double, dim>& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

            // store values of shape functions
            std::vector<RangeType> values(tFE.localBasis().size());

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const LocalCoordinate& quadPos = quad[pt].position();

                // get integration factor
                const double integrationElement = geometry.integrationElement(quadPos);

                // evaluate basis functions
                tFE.localBasis().evaluateFunction(quadPos, values);

                // compute values of function
                T f_pos;
                const GridFunction* gf = dynamic_cast<const GridFunction*>(&f_);
                if (gf and gf->isDefinedOn(element))
                    gf->evaluateLocal(element, quadPos, f_pos);
                else
                    f_.evaluate(geometry.global(quadPos), f_pos);

                // and vector entries
                for(size_t i=0; i<values.size(); ++i)
                {
                    localVector[i].axpy(values[i]*quad[pt].weight()*integrationElement, f_pos);
                }
            }
            return;
        }

    private:
        const Function& f_;
        const int quadOrder_;
        const QuadratureRuleKey functionQuadKey_;

        OscLocalProblemRegion<typename GridType::template Codim<0>::Entity, Dune::FieldVector<ctype, dimworld>>& region_;
};
#endif

