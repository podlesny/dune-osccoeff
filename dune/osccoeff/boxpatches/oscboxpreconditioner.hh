#ifndef OSC_BOX_PRECONDITIONER_HH
#define OSC_BOX_PRECONDITIONER_HH

#include <queue>
#include <string>

#include <dune/common/timer.hh>
#include <dune/common/fvector.hh>

#include <dune/solvers/iterationsteps/lineariterationstep.hh>

#include <dune/osccoeff/compressedmultigridtransfer.hh>
#include <dune/osccoeff/boxpatches/oscboxlocalassembler.hh>
#include <dune/osccoeff/boxpatches/oscboxlocalproblem.hh>
#include <dune/osccoeff/boxpatches/osclocalproblemregion.hh>
#include <dune/osccoeff/boxpatches/oscl2functionalassembler.hh>
#include <dune/osccoeff/hierarchicleveliterator.hh>
#include <dune/osccoeff/oscbasesolver.hh>

#include <dune/fufem/assemblers/assembler.hh>
#include <dune/fufem/boundarypatch.hh>

#include <dune/grid/common/mcmgmapper.hh>
#include <dune/grid/common/intersectioniterator.hh>

#include <dune/istl/superlu.hh>

template <class GridType, class BasisType, class MatrixType, class VectorType>
class OscBoxPreconditioner : public LinearIterationStep<MatrixType, VectorType> {

public:
    typedef GridType grid_type;
    typedef MatrixType matrix_type;

private:
    static const int dim= GridType::dimension;
    static const int dimworld = GridType::dimensionworld;
    typedef typename GridType::ctype ctype;
    typedef typename Dune::FieldVector<ctype, dimworld> WorldVectorType;
    typedef typename Dune::LevelMultipleCodimMultipleGeomTypeMapper <GridType,  Dune::MCMGElementLayout > MapperType;
    typedef typename GridType::template Codim<0>::Entity EntityType;

    typedef HierarchicLevelIterator<GridType> HierarchicLevelIteratorType;

    typedef OscBaseSolver<MatrixType, VectorType> BaseSolver;

    GridType& grid_;
    std::vector<ctype>& oscData_;
    
    const int coarseLevel_;
    const int fineLevel_;
    
    MapperType& mapper_;
    
    const ctype boxLength_;

    VectorType& fineRhs_;

    BaseSolver& baseSolver_;

    std::vector<OscBoxLocalProblem<BasisType, MatrixType, VectorType, VectorType>* > localProblems_;

    MatrixType coarseStiffMat_;
    VectorType coarseRhs_;
    Dune::BitSetVector<1> coarseBoundaryDofs_;
    CompressedMultigridTransfer<VectorType> mgTransfer_;

    struct classcomp {
    	bool operator() (const WorldVectorType& lhs, const WorldVectorType rhs) const {
	
	    for (size_t i=0; i<lhs.size(); ++i) {
		if (lhs[i]>rhs[i]) 
		    return false;
		else if (lhs[i]<rhs[i])
		    return true;
	    }
        return false;
    	}
    };
    
    void addRegionElements(const EntityType seed, std::vector<EntityType>& regionElements, int& elemCounter){

        HierarchicLevelIteratorType endHierIt(grid_, seed, HierarchicLevelIteratorType::PositionFlag::end, fineLevel_);
        for (HierarchicLevelIteratorType hierIt(grid_, seed, HierarchicLevelIteratorType::PositionFlag::begin, fineLevel_); hierIt!=endHierIt; ++hierIt) {

            EntityType entity(*hierIt);
            regionElements[elemCounter] = entity;
            elemCounter++;
        }
    }



public:

    OscBoxPreconditioner(GridType& grid, int coarseLevel, int fineLevel, std::vector<ctype>& oscData, MapperType& mapper, ctype boxLength, VectorType& fineRhs, BaseSolver& baseSolver)
        : grid_(grid), oscData_(oscData), coarseLevel_(coarseLevel), fineLevel_(fineLevel), mapper_(mapper), boxLength_(boxLength), fineRhs_(fineRhs), baseSolver_(baseSolver) {

        typedef typename GridType::LevelGridView GV;
        typedef typename BasisType::LocalFiniteElement FE;

        GV coarseGridView(grid_.levelGridView(coarseLevel_));
        GV fineGridView(grid_.levelGridView(fineLevel_));

        BasisType coarseBasis(coarseGridView);
        BasisType fineBasis(fineGridView);

	
	
        std::cout << "Setting coarse grid oscData:" << std::endl;
        Dune::LevelMultipleCodimMultipleGeomTypeMapper <GridType,  Dune::MCMGElementLayout > coarseMapper(grid_, coarseLevel_);

    	// allocate  a  vector  for  the  data
    	std::vector<ctype> coarseOscData(coarseMapper.size());

        typedef  typename GV::template Codim <0>::Iterator  ElementLevelIterator;

        std::map<WorldVectorType, EntityType, classcomp> regionSeeds;

    	ElementLevelIterator endElemIt = coarseGridView.template end <0>();
    	for (ElementLevelIterator  it = coarseGridView. template begin <0>(); it!=endElemIt; ++it) {
            const typename EntityType::Geometry geometry = it->geometry();

            // set region seed elements
            for (int i=0; i<geometry.corners(); i++) {
                WorldVectorType corner = geometry.corner(i);
                typename std::map<WorldVectorType, EntityType>::iterator hasCorner = regionSeeds.find(corner);

                if (hasCorner==regionSeeds.end()) {
                    EntityType entity(*it);
                    regionSeeds.insert(std::pair<WorldVectorType, EntityType>(corner, entity));
                }
            }


            // set coarseOscData
            int counter = 0;
            ctype sum = 0;

            HierarchicLevelIteratorType endHierIt(grid_, *it, HierarchicLevelIteratorType::PositionFlag::end, fineLevel_);
            for (HierarchicLevelIteratorType hierIt(grid_, *it, HierarchicLevelIteratorType::PositionFlag::begin, fineLevel_); hierIt!=endHierIt; ++hierIt) {

                sum += oscData_[mapper_.index(*hierIt)];
                counter++;
            }
            coarseOscData[coarseMapper.index(*it)] = sum/counter;
	   // std::cout << "elem: " << (int) mapper.map(*it) << "| "<< coarseOscData[coarseMapper.map(*it)] << std::endl;
        } 

        WorldVectorType zero(2);
        zero = 0;

        std::cout << "Assembling coarse grid stiffness matrix!" << std::endl;

        // assemble coarse stiffness matrix
        WorldVectorType midPoint(1/2);
        ctype box = 1;
        
        Assembler<BasisType, BasisType> coarseAssembler(coarseBasis, coarseBasis);
        OscLocalProblemRegionBox<typename GridType::template Codim<0>::Entity, WorldVectorType> coarseLocalProblemRegion(midPoint, box);
        OscBoxLocalAssembler<GridType, FE, FE> coarseLocalAssembler(coarseOscData, coarseMapper, coarseLocalProblemRegion);
        coarseAssembler.assembleOperator(coarseLocalAssembler, coarseStiffMat_);
    
    
        std::cout << "Setting coarse grid boundary conditions!" << std::endl;

        typedef typename MatrixType::row_type RowType;
        typedef typename RowType::ConstIterator ColumnIterator;

        // set coarse boundary conditions
        BoundaryPatch<typename GridType::LevelGridView> coarseBoundaryPatch(coarseGridView, true);
        constructBoundaryDofs(coarseBoundaryPatch, coarseBasis, coarseBoundaryDofs_);
        
        for(size_t i=0; i<coarseBoundaryDofs_.size(); i++) {
            if(!coarseBoundaryDofs_[i][0])
                continue;

            const RowType& row = coarseStiffMat_[i];

            ColumnIterator cIt    = row.begin();
            ColumnIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt) {
                coarseStiffMat_[i][cIt.index()] = 0;
            }
            coarseStiffMat_[i][i]=1;
        }

        mgTransfer_.setup(coarseGridView, fineGridView);
       
        localProblems_.resize(grid_.size(coarseLevel_, dim));

        std::cout << "Initializing local fine grid corrections! " << std::endl;



        // init local fine level corrections
        int count = 0;
        OscRhs<WorldVectorType, Dune::FieldVector<ctype,1>> f;
        Dune::BitSetVector<1> fineBoundaryDofs;
        BoundaryPatch<typename GridType::LevelGridView> fineBoundaryPatch(fineGridView, true);
        constructBoundaryDofs(fineBoundaryPatch, fineBasis, fineBoundaryDofs);

        typedef typename GV::template Codim <dim>::Iterator  VertexLevelIterator;

        Timer timer;
        timer.reset();
        timer.start();
        VertexLevelIterator endIt = coarseGridView.template end <dim>();
        for (VertexLevelIterator  it = coarseGridView.template begin <dim>(); it!=endIt; ++it) {

            const WorldVectorType vertex = it->geometry().corner(0);
            EntityType regionSeed(regionSeeds.at(vertex));

       /*     std::cout << vertex[0] << " " << vertex[1] << " " << std::endl;
            std::cout << regionSeed->geometry().center().two_norm() << std::endl;*/

            OscLocalProblemRegionBox<EntityType, WorldVectorType> fineLocalProblemRegion(vertex, boxLength_);

            std::vector<EntityType> regionElements(mapper_.size(), regionSeed);
            int elemCounter = 0;
            addRegionElements(regionSeed, regionElements, elemCounter);

            std::queue<EntityType> elementQueue;
            elementQueue.push(regionSeed);

            Dune::BitSetVector<1> visited(coarseMapper.size());
            visited.unsetAll();

            visited[coarseMapper.index(regionSeed)][0] = true;

            while (!elementQueue.empty()) {

                const EntityType elem = elementQueue.front();
                elementQueue.pop();

                typename GridType::LevelIntersectionIterator neighborIt = coarseGridView.ibegin(elem);
                typename GridType::LevelIntersectionIterator neighborItEnd = coarseGridView.iend(elem);
                for (; neighborIt!=neighborItEnd; ++neighborIt) {
                    const typename GridType::LevelIntersection& intersection = *neighborIt;

                    if (intersection.neighbor()) {
                        EntityType neighbor = intersection.outside();

                        bool inRegion;
                        fineLocalProblemRegion.evaluate(neighbor, inRegion);
                        int neighborId = coarseMapper.index(neighbor);


                        if (inRegion and !(visited[neighborId][0])) {
                            elementQueue.push(neighbor);
                            visited[neighborId][0] = true;
                            addRegionElements(neighbor, regionElements, elemCounter);
                        }
                    }
                }
            }

            regionElements.erase(regionElements.begin()+elemCounter, regionElements.end());

            OscBoxLocalAssembler<GridType, FE, FE> oscLocalAssembler(oscData_, mapper_, fineLocalProblemRegion);
            OscL2FunctionalAssembler<GridType, FE> oscLocalFunctionalAssembler(f, fineLocalProblemRegion);
            OscBoxLocalProblem<BasisType, MatrixType, VectorType, VectorType>* localProblem =  new OscBoxLocalProblem<BasisType, MatrixType, VectorType, VectorType>(regionElements, fineBasis, fineBoundaryDofs, oscLocalAssembler, fineRhs_, oscLocalFunctionalAssembler, baseSolver_);

            localProblems_[count] = localProblem;

            if ((count+1) % 10 == 0) {
                std::cout << '\r' << std::floor((count+1.0)/coarseBasis.size()*100) << " % done. Elapsed time: " << timer.elapsed() << " seconds. Predicted total setup time: " << timer.elapsed()/(count+1.0)*coarseBasis.size() << " seconds." << std::flush;
            }
            count++;
        }

        std::cout << std::endl;
        std::cout << "Total setup time: " << timer.elapsed() << " seconds." << std::endl;
	timer.stop();
    }


   virtual void setProblem(const MatrixType& mat, VectorType& x, const VectorType& rhs) {
        this->x_ = &x;
        this->rhs_ = &rhs;
        this->mat_ = Dune::stackobject_to_shared_ptr(mat);

        mgTransfer_.restrict(*(this->rhs_), coarseRhs_);
        for(size_t i=0; i<coarseBoundaryDofs_.size(); i++) {
            if(!coarseBoundaryDofs_[i][0])
                continue;

            coarseRhs_[i] = 0;
        }

        for (size_t i=0; i<localProblems_.size(); i++) {
            localProblems_[i]->updateRhs(*(this->rhs_));
        }
    }

    virtual void iterate() {

	    // compute exact coarse solution
        *(this->x_) = 0;	

        VectorType coarseSol(coarseStiffMat_.M());

        #if HAVE_SUPERLU
        Dune::InverseOperatorResult res;

        Dune::SuperLU<MatrixType> coarseSolver(coarseStiffMat_);
        coarseSolver.apply(coarseSol, coarseRhs_, res);
        #else
        baseSolver_.setProblem(coarseStiffMat_, coarseSol, coarseRhs_);
        baseSolver_.preprocess();
        baseSolver_.solve();
        #endif

    	mgTransfer_.prolong(coarseSol, *(this->x_));

        VectorType x;
        for (size_t i=0; i<localProblems_.size(); i++) {

            localProblems_[i]->solve(x);
            *(this->x_) += x;
        }
    }

};

#endif

