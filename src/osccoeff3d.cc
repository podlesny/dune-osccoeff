#include <config.h>

#include <cmath>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/timer.hh>
#include <dune/common/parallel/mpihelper.hh>

// dune-istl includes
#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/preconditioners.hh>

// dune-fufem includes
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/assemblers/assembler.hh>
#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/assemblers/localassemblers/l2functionalassembler.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>

// dune-grid includes
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/common/mcmgmapper.hh>

// dune-solver includes
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/norms/twonorm.hh>
#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/iterationsteps/cgstep.hh>

#include <dune/osccoeff/oscbasesolver.hh>
#include <dune/osccoeff/compressedmultigridtransfer.hh>
#include <dune/osccoeff/oscunitcube.hh>
#include <dune/osccoeff/osclocalassembler.hh>
#include <dune/osccoeff/matrixreader.hh>
#include <dune/osccoeff/oscrhs.hh>
#include <dune/osccoeff/supportpatches/oscsupportpreconditioner.hh>
#include <dune/osccoeff/osccgsolver.hh>

const int dim = 3;
typedef double ctype;

typedef typename Dune::FieldVector<ctype, dim> VectorType;
typedef typename Dune::BitSetVector<1> BitVector;
typedef typename Dune::BCRSMatrix<Dune::FieldMatrix<ctype, 1, 1>> MatrixType;

#if HAVE_UG
typedef typename Dune::UGGrid<dim> GridType;
typedef OscUnitCube<GridType, 2> GridOb;
#else 
#error No UG!
#endif

typedef typename GridType::LevelGridView GV;

typedef P1NodalBasis<GV, ctype> Basis;
typedef typename Basis::LocalFiniteElement FE;

int main(int argc, char** argv) { try {

    Dune::MPIHelper::instance(argc, argv);

  // parse parameter file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("osccoeff3d.parset", parameterSet);

    const std::string path = parameterSet.get<std::string>("path");
    const std::string resultPath = parameterSet.get<std::string>("resultPath");

    int problemCount = 0;
    while (parameterSet.hasSub("problem" + std::to_string(problemCount))) {
	
    ParameterTree& problemParameters = parameterSet.sub("problem" + std::to_string(problemCount));

    const std::string oscDataFile = problemParameters.get<std::string>("oscDataFile");

    const int coarseResolution = problemParameters.get<int>("coarseResolution");
    const int patchDepth = problemParameters.get<double>("patchDepth");
    const int fineResolution = problemParameters.get<int>("fineResolution");
    const int exactResolution = problemParameters.get<int>("exactResolution");

    const int maxIterations = problemParameters.get<int>("maxIterations");
    const double solverTolerance = problemParameters.get<double>("tol");

    const int baseIterations = problemParameters.get<int>("baseIterations");
    const double baseTolerance = problemParameters.get<double>("baseTol");

    const int coarseGridN = std::pow(2, coarseResolution);
    const int fineGridN = std::pow(2, fineResolution);

    std::ofstream out(resultPath + oscDataFile + "_" + std::to_string(coarseResolution) + "_" + std::to_string(fineResolution) + "_" + std::to_string(exactResolution) + ".log");
    std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
    std::cout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!

    std::cout << std::endl;
    std::cout << "Parameter file read successfully!" << std::endl;

    typedef Dune::Matrix<Dune::FieldMatrix<ctype,1,1>, std::allocator<Dune::FieldMatrix<ctype,1,1>>> OscMatrixType;
    OscMatrixType matrix;
    MatrixReader<OscMatrixType> matrixReader(path + oscDataFile);
    matrixReader.read(matrix);

    const int oscGridN = matrix.M();

    if (oscGridN>fineGridN) {
        std::cout << "Provided oscData too large!" << std::endl;
        DUNE_THROW(Dune::Exception, "Provided oscData too large!");
    }

    std::cout << "OscData file read successfully!" << std::endl << std::endl;

    // set unit square grid
    //Dune::UGGrid<dim>::setDefaultHeapSize(4000);
    GridOb unitCube(coarseGridN);
    GridType& grid = unitCube.grid();


    const int fineLevelIdx = fineResolution - coarseResolution;
    const int exactLevelIdx = exactResolution - coarseResolution;
    grid.globalRefine(exactLevelIdx);

    std::cout << "Coarse and fine grid were generated!" << std::endl;

    typedef  typename GV::Codim <0>::Iterator  ElementLevelIterator;
    typedef  typename ElementLevelIterator::Entity::Geometry  LevelGeometry;
    int i,j;
    
    Timer totalTimer;
    totalTimer.reset();
    totalTimer.start();
    // ----------------------------------------------------------------
    // ---              compute initial iterate                     ---
    // ----------------------------------------------------------------
    std::cout << std::endl;
    std::cout << "Computing initial iterate!" << std::endl;
 
    GV coarseGridView(grid.levelGridView(0));
    GV fineGridView(grid.levelGridView(fineLevelIdx));
    Basis coarseBasis(coarseGridView);

    std::cout << "Setting fine grid oscData:" << std::endl;

    Dune::LevelMultipleCodimMultipleGeomTypeMapper <GridType,  Dune::MCMGElementLayout > fineMapper(grid, fineLevelIdx);

    // allocate  a  vector  for  the  data
    std::vector<ctype> fineOscData(fineMapper.size());

    ElementLevelIterator endIt = fineGridView.end <0>();
    for (ElementLevelIterator  it = fineGridView.begin <0>(); it!=endIt; ++it) {
        const LevelGeometry  geometry = it->geometry();

        Dune::FieldVector <ctype, dim > global = geometry.center();
        i = oscGridN - std::ceil(global[0]*oscGridN) + oscGridN*(oscGridN - std::ceil(global[2]*oscGridN));
        j = std::floor(global[1]*oscGridN);
        fineOscData[fineMapper.index(*it)] = matrix[i][j];
    }

    std::cout << "Setting coarse grid oscData:" << std::endl;

    Dune::LevelMultipleCodimMultipleGeomTypeMapper <GridType,  Dune::MCMGElementLayout > coarseMapper(grid, 0);

    // allocate  a  vector  for  the  data
    std::vector<ctype> coarseOscData(coarseMapper.size());
 
    typedef HierarchicLevelIterator<GridType> HierarchicLevelIteratorType;

    typedef  typename GV::Codim <0>::Iterator  ElementLevelIterator;
    ElementLevelIterator endElemIt = coarseGridView.end <0>();
    for (ElementLevelIterator  it = coarseGridView.begin <0>(); it!=endElemIt; ++it) {
        int counter = 0;
        ctype sum = 0;
 	  
        HierarchicLevelIteratorType endHierIt(grid, *it, HierarchicLevelIteratorType::PositionFlag::end, fineLevelIdx);
        for (HierarchicLevelIteratorType hierIt(grid, *it, HierarchicLevelIteratorType::PositionFlag::begin, fineLevelIdx); hierIt!=endHierIt; ++hierIt) {
            sum += fineOscData[fineMapper.index(*hierIt)];
            counter++;
        }
        coarseOscData[coarseMapper.index(*it)] = sum/counter;
    } 
       

    // assemble stiffness matrix
    MatrixType coarseStiffMat;
    Assembler<Basis, Basis> coarseAssembler(coarseBasis, coarseBasis);

    OscLocalAssembler<GridType, FE, FE> coarseLocalAssembler(coarseOscData, coarseMapper);
    coarseAssembler.assembleOperator(coarseLocalAssembler, coarseStiffMat);

    // assemble rhs
    Dune::BlockVector<Dune::FieldVector<double,1>> coarseRhs, coarseSol, initialX;
    coarseRhs.resize(coarseStiffMat.N());
    coarseSol.resize(coarseStiffMat.M());

    OscRhs<VectorType, Dune::FieldVector<ctype,1>> f;
    L2FunctionalAssembler<GridType, FE> l2FunctionalAssembler(f);
    coarseAssembler.assembleFunctional(l2FunctionalAssembler, coarseRhs);

    typedef typename MatrixType::row_type RowType;
    typedef typename RowType::ConstIterator ColumnIterator;

    // set boundary conditions
    BitVector coarseBoundaryDofs;
    BoundaryPatch<GridType::LevelGridView> coarseBoundaryPatch(coarseGridView, true);
    constructBoundaryDofs(coarseBoundaryPatch, coarseBasis, coarseBoundaryDofs);

    for(size_t i=0; i<coarseBoundaryDofs.size(); i++) {
        if(!coarseBoundaryDofs[i][0])           
		continue;

        const RowType& row = coarseStiffMat[i];

        ColumnIterator cIt    = row.begin();
        ColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt) {
            coarseStiffMat[i][cIt.index()] = 0;
        }

        coarseStiffMat[i][i]=1;
        coarseRhs[i] = 0;
        coarseSol[i] = 0;
    }


    // compute coarse solution
    OscBaseSolver<MatrixType, Dune::BlockVector<Dune::FieldVector<ctype,1>>> baseSolver(baseIterations, baseTolerance, Solver::REDUCED);

    Timer timer;
    timer.reset();
    timer.start();

    baseSolver.setProblem(coarseStiffMat, coarseSol, coarseRhs);
    baseSolver.preprocess();
    baseSolver.solve();

    std::cout << "Solving coarse problem: " << timer.elapsed() << " seconds" << std::endl;
    timer.stop();
    timer.reset();	

    CompressedMultigridTransfer<Dune::BlockVector<Dune::FieldVector<ctype,1>>> mgTransfer;
    mgTransfer.setup(coarseGridView, fineGridView);
    mgTransfer.prolong(coarseSol, initialX);

    // ----------------------------------------------------------------
    // ---              compute exact solution                      ---
    // ----------------------------------------------------------------
    std::cout << std::endl;
    std::cout << "Computing exact solution!" << std::endl;

    GV exactGridView(grid.levelGridView(exactLevelIdx));
    Basis exactBasis(exactGridView);


        // attach ocsData to exact grid
        Dune::LevelMultipleCodimMultipleGeomTypeMapper <GridType,  Dune::MCMGElementLayout > exactMapper(grid, exactLevelIdx);

        // allocate  a  vector  for  the  data
        std::vector<ctype> exactOscData(exactMapper.size());

        ElementLevelIterator endIter = exactGridView.end <0>();
        for (ElementLevelIterator  it = exactGridView.begin <0>(); it!=endIter; ++it) {
            const LevelGeometry  geometry = it->geometry();

            Dune::FieldVector <ctype, dim > global = geometry.center();
            i = oscGridN - std::ceil(global[0]*oscGridN);
            j = std::floor(global[1]*oscGridN);
            exactOscData[exactMapper.index(*it)] = matrix[i][j];
        }

        // assemble stiffness matrix
        MatrixType exactStiffMat;
        Assembler<Basis, Basis> exactAssembler(exactBasis, exactBasis);
   
        OscLocalAssembler<GridType, FE, FE> oscExactAssembler(exactOscData, exactMapper);
        exactAssembler.assembleOperator(oscExactAssembler, exactStiffMat);

        // assemble rhs
        Dune::BlockVector<Dune::FieldVector<double,1>> rhs, exactSol, exactX;
        rhs.resize(exactStiffMat.N());
        exactSol.resize(exactStiffMat.M());

        exactAssembler.assembleFunctional(l2FunctionalAssembler, rhs);

        // set boundary conditions
        BitVector boundaryDofs;
        BoundaryPatch<GridType::LevelGridView> boundaryPatch(exactGridView, true);
        constructBoundaryDofs(boundaryPatch, exactBasis, boundaryDofs);

        for(size_t i=0; i<boundaryDofs.size(); i++) {
            if(!boundaryDofs[i][0])
                continue;

            const RowType& row = exactStiffMat[i];

            ColumnIterator cIt    = row.begin();
            ColumnIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt) {
                exactStiffMat[i][cIt.index()] = 0;
            }

            exactStiffMat[i][i]=1;
            rhs[i] = 0;
            exactSol[i] = 0;
        }

        // compute exact solution
        timer.start();

        baseSolver.setProblem(exactStiffMat, exactSol, rhs);
        baseSolver.preprocess();
        baseSolver.solve();

        std::cout << "Solving exact problem: " << timer.elapsed() << " seconds" << std::endl;
        timer.stop();
        timer.reset();


        mgTransfer.setup(fineGridView, exactGridView);
        mgTransfer.restrict(exactSol, exactX);

        EnergyNorm<MatrixType, Dune::BlockVector<Dune::FieldVector<ctype,1>> > exactEnergyNorm(exactStiffMat);

    // ----------------------------------------------------------------
    // ---                set up cg solver                          ---
    // ----------------------------------------------------------------
    std::cout << std::endl;
    std::cout << "---------------------------------------------" << std::endl; 
    std::cout << "Setting up the fine level CG solver:" << std::endl; 
    std::cout << "---------------------------------------------" << std::endl << std::endl;


    std::cout << "Assembling stiffness matrix" << std::endl;
    
  
    Basis fineBasis(fineGridView);

    // assemble stiffness matrix
    MatrixType fineStiffMat;
    Assembler<Basis, Basis> fineAssembler(fineBasis, fineBasis);

    OscLocalAssembler<GridType, FE, FE> oscFineAssembler(fineOscData, fineMapper);
    fineAssembler.assembleOperator(oscFineAssembler, fineStiffMat);

    std::cout << "Assembling right hand side" << std::endl;
    // assemble rhs
    Dune::BlockVector<Dune::FieldVector<double,1>> fineRhs, fineX;
    fineRhs.resize(fineStiffMat.N());
    fineX.resize(fineStiffMat.M());

    fineAssembler.assembleFunctional(l2FunctionalAssembler, fineRhs);
    
    std::cout << "Setting boundary conditions" << std::endl;
    // set boundary conditions
    BitVector fineBoundaryDofs;
    BoundaryPatch<GridType::LevelGridView> fineBoundaryPatch(fineGridView, true);
    constructBoundaryDofs(fineBoundaryPatch, fineBasis, fineBoundaryDofs);
    for(size_t i=0; i<fineBoundaryDofs.size(); i++) {
        if(!fineBoundaryDofs[i][0])
            continue;

        const RowType& row = fineStiffMat[i];

        ColumnIterator cIt    = row.begin();
        ColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt) {
            fineStiffMat[i][cIt.index()] = 0;
        }

        fineStiffMat[i][i] = 1;
        fineRhs[i] = 0;
	fineX[i] = 0;
    exactX[i] = 0;
	initialX[i] = 0;
    }

    // compute exact solution on fine grid
    std::cout << "Computing the fine solution" << std::endl;

    Dune::BlockVector<Dune::FieldVector<double,1>> fineRhsClone(fineRhs);

    baseSolver.setProblem(fineStiffMat, fineX, fineRhsClone);
    baseSolver.preprocess();
    baseSolver.solve();

    EnergyNorm<MatrixType, Dune::BlockVector<Dune::FieldVector<ctype,1>> > energyNorm(fineStiffMat);

    Dune::BlockVector<Dune::FieldVector<ctype,1>> initialXCopy(initialX);
    Dune::BlockVector<Dune::FieldVector<ctype,1>> fineRhsCopy(fineRhs);

    std::cout << "Setting up the preconditioner!" << std::endl;
  
    baseSolver.setVerbosity(Solver::QUIET);
    OscSupportPreconditioner<GridType, Basis, MatrixType, Dune::BlockVector<Dune::FieldVector<ctype,1>> > preconditioner(grid, 0, fineLevelIdx, fineOscData, fineMapper, patchDepth, fineStiffMat, fineRhs, baseSolver, false);

    std::cout << "Setup complete, starting preconditioned cg iteration!" << std::endl;
    std::cout << std::endl << std::endl;

    // solve
    OscCGSolver<MatrixType, Dune::BlockVector<Dune::FieldVector<ctype,1>> >
    solver(&fineStiffMat, &initialX, &fineRhs, &preconditioner,
                    maxIterations, solverTolerance, &exactEnergyNorm,
                    Solver::FULL, &exactSol, &fineX, &mgTransfer, 1.0, true); //((oscGridN+0.0)/fineGridN)
    solver.check();
    solver.preprocess();
    solver.solve();

    Dune::BlockVector<Dune::FieldVector<ctype,1>> finalError(fineX.size());
    finalError = initialX;
    finalError -= fineX;
    std::cout << "Final error in energy norm: " << energyNorm.operator()(finalError) << std::endl;
    std::cout << "Final error in two norm: " << finalError.two_norm() << std::endl << std::endl;

    std::cout << std::endl;
    std::cout << "Total time: " << totalTimer.elapsed() << " seconds." << std::endl; 

    std::cout.rdbuf(coutbuf); //reset to standard output again

    std::cout << "Problem " << problemCount << " done!" << std::endl;
    problemCount++;
    }
} catch (Dune::Exception &e) {
    Dune::derr << "Dune reported error: " << e << std::endl;
} catch (std::exception &e) {
    std::cerr << "Standard exception: " << e.what() << std::endl;
}
}
